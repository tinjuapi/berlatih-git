1. Buat Database
CREATE DATABASE myshop;

2. Membuat Table Baru


CREATE TABLE users( id int(4) AUTO_INCREMENT PRIMARY KEY, name varchar(255), email varchar(255), password varchar(255) );

CREATE TABLE categories( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) );

CREATE TABLE items( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price int NOT null, stock int NOT null, category_id int NOT null, FOREIGN KEY(category_id) REFERENCES categories(id) );



3. Masukkan data

tabel user:

INSERT INTO users (name, email, password) VALUES("John Doe", "john@doe.com", "john123"), ("John Doe", "john@doe.com", "jenita123");

tabel categories:

INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

tabel items:

INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", "4000000" , 100, 1), ("Uniklooh", "baju keren dari brand ternama", "500000", 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", 20, 1);

4. Tampilkan data
 a. mengambil data kecuali password

SELECT id, name, email from users;



b. Mengambil data items 

SELECT * FROM `items` WHERE price > 1000000;

SELECT * FROM `items` WHERE name LIKE '%watch';

c. Menampilkan data items join dengan kategori

SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items INNER JOIN categories ON items.category_id = categories.id;



5. Mengubah Data dari Database


UPDATE items SET price = 2500000 WHERE id = 1;











