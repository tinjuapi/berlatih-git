<?php

require_once('frog.php');
require_once('animal.php');
require_once('ape.php');

$sheep = new Animal('shaun');
echo 'name = '. $sheep->name . '<br>';
echo 'legs = '. $sheep->legs . '<br>'; 
echo 'cold blooded = '. $sheep->cold_blooded . '<br> <br>'; 

$frog = new Kodok('buduk');
echo 'name = ' . $frog->name . '<br>';
echo 'legs = ' . $frog->legs . '<br>'; 
echo 'cold blooded = ' . $frog->cold_blooded . '<br>';
echo 'jump = ' . $frog->jump . '<br><br>'; 

$ape = new Sungokong('kera sakti');
echo 'name = ' . $ape->name . '<br>';
echo 'legs = ' . $ape->legs . '<br>'; 
echo 'cold blooded = ' . $ape->cold_blooded . '<br>';
echo 'Yell = ' . $ape->yell . '<br>'; 




?>